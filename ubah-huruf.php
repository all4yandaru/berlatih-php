<?php
function ubah_huruf($string){
	$length = strlen($string);
	$ord;

	for ($i=0; $i < $length; $i++) {
		$ord = ord($string[$i]) + 1;
		$string[$i] = chr($ord);
	}
	return $string . "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>